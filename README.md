# Czokalapik's marker packs for GW2 Taco  
![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_hp_run_screen.png)
Like many of you, I'm using GW2 Taco, it's amazing overlay addon for GW2, markers were intended to be created by players just like me and you, so here is my input. This is collection of marker packs made by me, for my own use, but since this work helped me greatly in GW2 I want to share it so hopefully it will help you too.

#### Quick start  
1. If you don't have Taco yet, follow the guides on [how to install it](http://www.gw2taco.com/2015/12/quick-start-guide.html)
2. To install my marker pack, [download](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/downloads/) latest build
3. Extract downloaded files to GW2 Taco install folder and overwrite POIs folder (no changes to other packs will be made). Putting unpacked zip in POIs folder probably won't load images, only trails.

## Hero Points train guide  
This one is first one I made because after completing 3 stories and following few HP trains I still didn't know my way around the HoT maps. This marker pack is designed for Commanders leading HP trains and/or solo players (with small requirements). Very **few challenges require Springer**, original routes requiring only HoT masteries and/or gliding are not included for some Hero Challenges (for example Glided River), those are very long and too iritating to include here.

### What is required:
* **Glider** with **Updraft Use** mastery
* **Bouncing Mushrooms** mastery (**Springer** can replace it in most cases)
* **Itzel Poison Lore** mastery for Chak Hatchery HP
* **Springer** lvl3 to use some shortcuts
* **Mesmer's Portal** if you want to lead HP train

Optional, but not required: **Ley Line Gliding** and **Lean Techniques**.

### Routes shown on maps:
* **HoT**
	* [Verdant Brink](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/VB.jpg)
	* [Auric Basin](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/AB.jpg)
	* [Tangled Depths](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/TD.jpg)

### Marks explanation
Mark  | Description 
-: | :-
![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_portal.png)  | Every spot where you have to use Bouncing Mushrooms, Ley Line Gliding, Updraft Use, or Springer is marked with spots where you should use **Portal** to help other members of your group.
![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_num_label.png) | Every HP trail starts with corresponding number (at Waypoint or after previous HP), same icon appears on every fork in the road. Labels under the number will tell where you are going, you will see Hero Challenge name or Waypoint name here, "ALT ROUTE" on the label means this is alternative route to this Hero Challenge. You'll also see several icons on the label: 
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_hp.png) - This route leads to soloable Hero Point, same icon with "+" means HP that will require 2 or more players
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_wp.png) - This trail leads to Waypoint
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_exaltedmarkings.png) - You will need Exalted Markings Mastery on the way (or something to skip them), needed only for Balthazar HP, Jellyfish and Chak Hatchery HPs
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_itzelpoison.png) - Itzel Poison mastery is reqiured here (only Chak Hatchery HP)
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_leyline.png) - Ley-Line mastery is required to get to this HP (**Springer** or even  **Advanced Gliding** be used instead, only Burnisher's Quarry vanilla path)
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_mushroom.png) - Bouncing Mushroom mastery is required here, usually **Springer** can be used instead
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_updraft.png) - Updraft mastery is required to follow this route, usually **Springer** can be used instead
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_lean.png) - Lean Techniques mastery required (**Springer** can be used instead)
| ![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_labels_springer.png) - Springer is required to follow this route
![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_trail.png) | **Yellow trail** leads directly to Hero Challenge. **Blue trail** leads to next Waypont, you will see yellow continuation of the trial after getting to WP.
![](https://bitbucket.org/czokalapik/czokalapiks-guides-for-gw2taco/raw/master/POIs/Data/CzokalapiksGuides/git-instructions/cz_git_signs_tp_to.png) | This mark will tell you where to teleport after completing Hero Challenge (if the trail is not leadeing you to next HP). You will only be pointed to starting Waypoint or the ones this route led you already.

### Changelog
* 22.05.2018: Connected Verdant Brink's Golem HP with Ancient Tree HP
* 21.05.2018: Updated Vampire HP trail

### ToDo
* **PoF** maps (maybe?)